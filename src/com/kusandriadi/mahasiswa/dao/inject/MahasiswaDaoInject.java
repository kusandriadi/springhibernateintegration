package com.kusandriadi.mahasiswa.dao.inject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.kusandriadi.mahasiswa.bean.Mahasiswa;
import com.kusandriadi.mahasiswa.dao.MahasiswaDao;

public class MahasiswaDaoInject extends HibernateDaoSupport implements MahasiswaDao{
	
	@SuppressWarnings("unchecked")
	@Override
	public String autoNumber() {
		List objectResult = getHibernateTemplate().find(
				"select max(nim) as nim from Mahasiswa");
		
		Mahasiswa mahasiswa = null;
		
		for (Object resultElement : objectResult){
			mahasiswa = new Mahasiswa();
			mahasiswa.setNim((String)resultElement);
		}

		return mahasiswa.getNim();
	}

	@Override
	@Transactional(readOnly=false, propagation=Propagation.REQUIRES_NEW)
	public void deleteMahasiswa(Mahasiswa mahasiswa) {
		try {
			getHibernateTemplate().delete(mahasiswa);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	@Transactional(readOnly=false, propagation=Propagation.REQUIRES_NEW)
	public void insertMahasiswa(Mahasiswa mahasiswa) {
		try {
			getHibernateTemplate().save(mahasiswa);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public ArrayList<Mahasiswa> moveDataFromTable(String nim) {
		try {
			List<Mahasiswa> list = getHibernateTemplate().find(
					"FROM Mahasiswa WHERE nim = ?", nim);
			
			ArrayList<Mahasiswa> maha = new ArrayList<Mahasiswa>();
			Mahasiswa mahasiswa = null;
			for(Mahasiswa mhs : list){
				mahasiswa = new Mahasiswa();
				mahasiswa.setNim(mhs.getNim());
				mahasiswa.setNama(mhs.getNama());
				mahasiswa.setTempat(mhs.getTempat());
				mahasiswa.setTanggal_lahir(mhs.getTanggal_lahir());
				mahasiswa.setJurusan(mhs.getJurusan());
				mahasiswa.setJenkel(mhs.getJenkel());
				mahasiswa.setAlamat(mhs.getAlamat());
				maha.add(mahasiswa);
			}
			return maha;
		} catch (HibernateException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	@Transactional(readOnly=true)
	public ArrayList<Mahasiswa> selectAll() {
		try {
			return (ArrayList<Mahasiswa>) getHibernateTemplate().loadAll(Mahasiswa.class);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	@Transactional(readOnly=false, propagation=Propagation.REQUIRES_NEW)
	public void updateMahasiswa(Mahasiswa mahasiswa) {
		try {
			getHibernateTemplate().update(mahasiswa);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public Mahasiswa getMahasiswa(final String mahasiswa) {
		try {
			Object obj = getHibernateTemplate().execute(new HibernateCallback() {
				
				@Override
				public Object doInHibernate(Session session) throws HibernateException,
						SQLException {
					return session.createQuery("from Mahasiswa where nim = :nim").setParameter("nim", mahasiswa).uniqueResult();
				}
			});
			if(obj == null){
				throw new Exception("Gagal Menghapus data mahasiswa");
			}else{
				return (Mahasiswa) obj;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
