package com.kusandriadi.mahasiswa.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.kusandriadi.mahasiswa.dao.MahasiswaDao;

public class SpringUtilities {
	
	private static ApplicationContext applicationContext;
	
	public static ApplicationContext getApplicationContext(){
		applicationContext = new ClassPathXmlApplicationContext("com/kusandriadi/mahasiswa/conf/mahasiswa.xml");
		return applicationContext;
	}
	
	public static MahasiswaDao getMahasiswaDao(){
		return (MahasiswaDao)getApplicationContext().getBean("mahasiswaDao");
	}

}
