package com.kusandriadi.mahasiswa.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Mahasiswa")
public class Mahasiswa {
	
	@Id
	@Column(name = "nim", nullable = false, length = 10)
	private String nim;
	
	@Column(name = "nama",length = 30)
	private String nama;
	
	@Column(name = "tempat",length = 10)
	private String tempat;
	
	@Column(name = "tanggal_lahir")
	private Date tanggal_lahir;
	
	@Column(name = "jurusan")
	private String jurusan;
	
	@Column(name = "jenkel")
	private String jenkel;
	
	@Column(name = "alamat")
	private String alamat;

	public String getNim() {
		return nim;
	}

	public void setNim(String nim) {
		this.nim = nim;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getTempat() {
		return tempat;
	}

	public void setTempat(String tempat) {
		this.tempat = tempat;
	}

	public Date getTanggal_lahir() {
		return tanggal_lahir;
	}

	public void setTanggal_lahir(Date tanggalLahir) {
		tanggal_lahir = tanggalLahir;
	}

	public String getJurusan() {
		return jurusan;
	}

	public void setJurusan(String jurusan) {
		this.jurusan = jurusan;
	}

	public String getJenkel() {
		return jenkel;
	}

	public void setJenkel(String jenkel) {
		this.jenkel = jenkel;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alamat == null) ? 0 : alamat.hashCode());
		result = prime * result + ((jenkel == null) ? 0 : jenkel.hashCode());
		result = prime * result + ((jurusan == null) ? 0 : jurusan.hashCode());
		result = prime * result + ((nama == null) ? 0 : nama.hashCode());
		result = prime * result + ((nim == null) ? 0 : nim.hashCode());
		result = prime * result
				+ ((tanggal_lahir == null) ? 0 : tanggal_lahir.hashCode());
		result = prime * result + ((tempat == null) ? 0 : tempat.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mahasiswa other = (Mahasiswa) obj;
		if (alamat == null) {
			if (other.alamat != null)
				return false;
		} else if (!alamat.equals(other.alamat))
			return false;
		if (jenkel == null) {
			if (other.jenkel != null)
				return false;
		} else if (!jenkel.equals(other.jenkel))
			return false;
		if (jurusan == null) {
			if (other.jurusan != null)
				return false;
		} else if (!jurusan.equals(other.jurusan))
			return false;
		if (nama == null) {
			if (other.nama != null)
				return false;
		} else if (!nama.equals(other.nama))
			return false;
		if (nim == null) {
			if (other.nim != null)
				return false;
		} else if (!nim.equals(other.nim))
			return false;
		if (tanggal_lahir == null) {
			if (other.tanggal_lahir != null)
				return false;
		} else if (!tanggal_lahir.equals(other.tanggal_lahir))
			return false;
		if (tempat == null) {
			if (other.tempat != null)
				return false;
		} else if (!tempat.equals(other.tempat))
			return false;
		return true;
	}
}
