package com.kusandriadi.mahasiswa.tablemodel;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import com.kusandriadi.mahasiswa.bean.Mahasiswa;

public class TableMahasiswaModel extends AbstractTableModel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2081036189961494731L;
	
	ArrayList<Mahasiswa> list;
	
	public TableMahasiswaModel(ArrayList<Mahasiswa> list){
		this.list = list;
	}
	
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getValueAt(int row, int column) {
		// TODO Auto-generated method stub
		switch (column) {
		case 0:
			return list.get(row).getNim();
		case 1:
			return list.get(row).getNama();
		default:
			return null;
		}
	}

	@Override
	public String getColumnName(int column) {
		switch (column) {
		case 0:
			return "NIM";
		case 1:
			return "Mahasiswa";
		default:
			return null;
		}
	}
}
