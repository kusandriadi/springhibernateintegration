package com.kusandriadi.mahasiswa.main;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class MahasiswaMain extends JFrame{

	private static final long serialVersionUID = 1L;
	
	public MahasiswaMain(){
		super("Contoh CRUD");
		Dimension tengah = Toolkit.getDefaultToolkit().getScreenSize();
		setSize(670, 370);
		setLocation((tengah.width - getWidth())/2, (tengah.height - getHeight())/2);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		add(new MahasiswaPanel());
		setResizable(false);
		setVisible(true);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try{
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
			}catch(ClassNotFoundException e){
				
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnsupportedLookAndFeelException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		new MahasiswaMain();
	}

}
