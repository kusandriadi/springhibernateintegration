package com.kusandriadi.mahasiswa.dao;

import java.util.ArrayList;

import com.kusandriadi.mahasiswa.bean.Mahasiswa;

public interface MahasiswaDao {
	
	public void insertMahasiswa(Mahasiswa mahasiswa);
	
	public void updateMahasiswa(Mahasiswa mahasiswa);
	
	public void deleteMahasiswa(Mahasiswa mahasiswa);
	
	public Mahasiswa getMahasiswa(String mahasiswa);
	
	public ArrayList<Mahasiswa> selectAll();
	
	public ArrayList<Mahasiswa> moveDataFromTable(String nim);
	
	public String autoNumber();
}
