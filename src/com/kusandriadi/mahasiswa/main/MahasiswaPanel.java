package com.kusandriadi.mahasiswa.main;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.table.TableModel;

import com.kusandriadi.mahasiswa.bean.Mahasiswa;
import com.kusandriadi.mahasiswa.dao.MahasiswaDao;
import com.kusandriadi.mahasiswa.tablemodel.TableMahasiswaModel;
import com.kusandriadi.mahasiswa.util.SpringUtilities;
import com.toedter.calendar.JDateChooser;

public class MahasiswaPanel extends JPanel implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JButton btnSave 	= new JButton("Save"),
					btnClear 	= new JButton("Clear"),
					btnUpdate 	= new JButton("Update"),
					btnDelete 	= new JButton("Delete");

	private JLabel 	lblNim = new JLabel("NIM : "),
					lblNama = new JLabel("Nama : "),
					lblTTL = new JLabel("TTL : "),
					lblJur = new JLabel("Jurusan : "),
					lblJenkel = new JLabel("Jenis Kelamin : "),
					lblAlamat = new JLabel("Alamat : "),
					lblRead = new JLabel("*) klik 2x pada table untuk read data");
	
	private JTextArea arAlamat = new JTextArea(5, 30);
	
	private JTable tblMahasiswa = new JTable();
	
	private JScrollPane arScroll = new JScrollPane(arAlamat,
							ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
							ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER),
						tblScroll = new JScrollPane(tblMahasiswa,
							ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
							ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

	private JTextField 	txtNIM 	= new JTextField(),
						txtNama = new JTextField(),
						txtTmpt = new JTextField();
	
	private String Jurusan[] = { "--Pilih Jurusan--", "FTI", "FE", "FISIP" };
	
	private String Kelamin[] = { "--Jenis Kelamin--", "Pria", "Wanita" };
	
	private JComboBox cmbJurusan = new JComboBox(Jurusan),
		cmbJenkel = new JComboBox(Kelamin);
	
	
	private JDateChooser dateChooser = new JDateChooser();

	//private static String simpan = new String();
	
	public MahasiswaPanel(){
		setLayout(null);
		
		start();
		updateTable();
		autoNumber();
		lblNim.setBounds(20, 20, 90, 20);
		lblNama.setBounds(20, 60, 90, 20);
		lblTTL.setBounds(20, 100, 90, 20);
		lblJur.setBounds(20, 140, 90, 20);
		lblJenkel.setBounds(20, 180, 90, 20);
		lblAlamat.setBounds(20, 220, 90, 20);
		lblRead.setBounds(410, 20, 220, 20);
		
		lblNim.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNama.setHorizontalAlignment(SwingConstants.RIGHT);
		lblJur.setHorizontalAlignment(SwingConstants.RIGHT);
		lblJenkel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAlamat.setHorizontalAlignment(SwingConstants.RIGHT);
		lblTTL.setHorizontalAlignment(SwingConstants.RIGHT);
		lblRead.setHorizontalAlignment(SwingConstants.RIGHT);
		
		txtNIM.setBounds(120, 20, 80, 25);
		txtNama.setBounds(120, 60, 120, 25);
		txtTmpt.setBounds(120, 100, 80, 25);
		
		tblMahasiswa.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount() == 2){
					TableModel model = tblMahasiswa.getModel();
					String simpan = model.
						getValueAt(tblMahasiswa.getSelectedRow(), 0).
						toString();
					MahasiswaDao mahasiswaDao = SpringUtilities.getMahasiswaDao();
					ArrayList<Mahasiswa> mahasiswa = mahasiswaDao.moveDataFromTable(simpan);
					for(Mahasiswa mhs : mahasiswa){
						txtNIM.setText(mhs.getNim());
						txtNama.setText(mhs.getNama());
						txtTmpt.setText(mhs.getTempat());
						dateChooser.setDate(mhs.getTanggal_lahir());
						cmbJenkel.setSelectedItem(mhs.getJenkel());
						cmbJurusan.setSelectedItem(mhs.getJurusan());
						arAlamat.setText(mhs.getAlamat());
					}
					btnSave.setEnabled(false);
					btnUpdate.setEnabled(true);
					btnDelete.setEnabled(true);
				}
			}
			
		});
		
		dateChooser.setCalendar(Calendar.getInstance());
		dateChooser.setDateFormatString("dd-MMM-yyyy");
		dateChooser.setBounds(210, 100, 130, 25);
		
		cmbJurusan.setBounds(120, 140, 120, 25);
		
		cmbJenkel.setBounds(120, 180, 130, 25);
		
		arScroll.getViewport();
		arAlamat.setLineWrap(true);
		arAlamat.setWrapStyleWord(true);
		arScroll.setBounds(120, 220, 160, 60);
		
		btnClear.setBounds(20, 290, 80, 25);
		btnSave.setBounds(120, 290, 80, 25);
		btnUpdate.setBounds(220, 290, 80, 25);
		btnDelete.setBounds(320, 290, 80, 25);
		
		btnSave.addActionListener(this);
		btnClear.addActionListener(this);
		btnUpdate.addActionListener(this);
		btnDelete.addActionListener(this);
		
		tblScroll.getViewport();
		tblScroll.setBounds(430, 50, 220, 270);
		
		lblNim.setFont(new Font(getFont().getName(), Font.BOLD, getFont().getSize()));
		lblNama.setFont(new Font(getFont().getName(), Font.BOLD, getFont().getSize()));
		lblJur.setFont(new Font(getFont().getName(), Font.BOLD, getFont().getSize()));
		lblTTL.setFont(new Font(getFont().getName(), Font.BOLD, getFont().getSize()));
		lblJenkel.setFont(new Font(getFont().getName(), Font.BOLD, getFont().getSize()));
		lblAlamat.setFont(new Font(getFont().getName(), Font.BOLD, getFont().getSize()));
		
		//masukan komponen label
		add(lblNim);
		add(lblNama);
		add(lblTTL);
		add(lblJur);
		add(lblJenkel);
		add(lblAlamat);
		add(lblRead);
		
		//masukan komponen textfield
		add(txtNIM);
		add(txtNama);
		add(txtTmpt);
		
		//date chooser
		add(dateChooser);
		
		//combobox
		add(cmbJurusan);
		add(cmbJenkel);
		
		//alamat
		add(arScroll);
		
		//button
		add(btnClear);
		add(btnUpdate);
		add(btnDelete);
		add(btnSave);
		
		add(tblScroll);
		
	}
	
	private void start() {
		btnUpdate.setEnabled(false);
		btnDelete.setEnabled(false);
		txtNIM.setEnabled(false);
		btnSave.setEnabled(true);
	}
	
	private void clear(){
		txtNIM.setText("");
		txtNama.setText("");
		txtTmpt.setText("");
		dateChooser.setCalendar(Calendar.getInstance());
		cmbJurusan.setSelectedIndex(0);
		cmbJenkel.setSelectedIndex(0);
		arAlamat.setText("");
	}
	
	private void setting(){
		start();
		clear();
		autoNumber();
	}
	
	private void updateTable(){
		MahasiswaDao mahasiswaDao = SpringUtilities.getMahasiswaDao();
		ArrayList<Mahasiswa> list = mahasiswaDao.selectAll();
		TableModel model = new TableMahasiswaModel(list);
		tblMahasiswa.setModel(model);
	}
	
	private void autoNumber() {
		MahasiswaDao mahasiswaDao = SpringUtilities.getMahasiswaDao();
		String auto = mahasiswaDao.autoNumber();
		if (auto == null) {
			txtNIM.setText("M0001");
		} else {
			auto = auto.substring(1);
			int angka = Integer.parseInt(auto);
			angka = angka + 1;
			if (angka <= 9) {
				auto = "000" + angka;
			} else if (angka >= 10 && angka <= 99) {
				auto = "00" + angka;
			} else if (angka >= 100 && angka <= 999) {
				auto = "0" + angka;
			} else if (angka >= 1000 && angka <= 9999) {
				auto = "" + angka;
			}
			if (auto.length() > 5) {
				auto = auto.substring(auto.length() - 5, auto.length());
			}
			auto = "M" + auto;
			txtNIM.setText(auto);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if(obj == btnSave){
			Mahasiswa mahasiswa = new Mahasiswa();
			mahasiswa.setNim(txtNIM.getText());
			mahasiswa.setNama(txtNama.getText());
			mahasiswa.setTempat(txtTmpt.getText());
			mahasiswa.setTanggal_lahir(dateChooser.getDate());
			mahasiswa.setJurusan((String)cmbJurusan.getSelectedItem());
			mahasiswa.setJenkel((String)cmbJenkel.getSelectedItem());
			mahasiswa.setAlamat(arAlamat.getText());
			
			MahasiswaDao mahasiswaDao = SpringUtilities.getMahasiswaDao();
			mahasiswaDao.insertMahasiswa(mahasiswa);
			updateTable();
			setting();
		}
		if(obj == btnClear){
			clear();
			setting();
		}
		if(obj == btnUpdate){
			Mahasiswa mahasiswa = new Mahasiswa();
			mahasiswa.setNim(txtNIM.getText());
			mahasiswa.setNama(txtNama.getText());
			mahasiswa.setTempat(txtTmpt.getText());
			mahasiswa.setTanggal_lahir(dateChooser.getDate());
			mahasiswa.setJurusan((String)cmbJurusan.getSelectedItem());
			mahasiswa.setJenkel((String)cmbJenkel.getSelectedItem());
			mahasiswa.setAlamat(arAlamat.getText());
			
			MahasiswaDao mahasiswaDao = SpringUtilities.getMahasiswaDao();
			mahasiswaDao.updateMahasiswa(mahasiswa);
			updateTable();
			setting();
		}
		if(obj == btnDelete){
			MahasiswaDao mahasiswaDao = SpringUtilities.getMahasiswaDao();
			
			Mahasiswa mahasiswa = mahasiswaDao.getMahasiswa(txtNIM.getText());
			mahasiswaDao.deleteMahasiswa(mahasiswa);
			updateTable();
			setting();
		}
	}
	
}
